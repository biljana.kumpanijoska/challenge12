$(function() {  

	function Person(fname, lname, avatar){
		this.fname=fname;
		this.lname=lname;
		this.avatar=avatar;		
	}

	var friends=[ new Person('Milena', 'Jovanoska', './images/person1.jpg'),
				new Person('Ognen', 'Milosheski', './images/person2.jpg'),
				new Person('Tijana', 'Kraleva', './images/person3.jpg'),
				new Person('Boban', 'Andreeski', './images/person4.jpg'),
				new Person('Vasil', 'Taneski', './images/person5.jpg'),
				new Person('Bojana', 'Vasev', './images/person6.jpg'),
				// new Person('Bojana', 'Vasev', './images/person7.jpg')
			  ];
	var me=new Person('Biljana', 'Kumpanijoska', './images/biljana.jpg');

	function addNewFriend2(name, lastname, avatar){
	//create div
		$("<div>").attr({
		    'class': "msg-container",
		}).appendTo(".members-list");

		$("<img>").attr({
		    'src': avatar,
		    'alt': 'avatar',
		    'class': 'right',
		}).appendTo($('.msg-container').last());

		$('.msg-container').last().append("<p><span id='friend-name'>"+name+" "+lastname+"</span></p>");
		$('.msg-container').last().append("<p><span class='small time-left' > Last time seen online</span><br><span class='time-seen'>Today, 11:01</span> </p>");
	}
	function MakeFriendsList2(){

		var index=0;
		$.each(friends, function(){
			addNewFriend2(friends[index].fname, friends[index].lname,friends[index].avatar);	
			index++;	
		});
			
	}
	MakeFriendsList2();
	function areYouSure(){
		var new_conversation = confirm ('Are you sure you want to cancel this conversation and start a new one?');
			if( new_conversation == true )
				closeConversation();
	}

	$('.msg-container').on('click', function(){

		if ( $(this).siblings().hasClass('active-conversation')  ) {
			areYouSure();
		}

		prevActive();
		$(this).addClass('active-conversation');

		// alert('pocnuvam razgovor');

		var imgsrc = $(this).children("img").attr('src');
		var friendname = $(this).find('#friend-name').text();
		// console.log(imgsrc);
		$('.header-conversation').children('img').attr({
		    'src': imgsrc,
		    'width':'32px',
		    'margin-left':'3%',
		    'alt': 'avatar',
		    'class': 'left',
		});
		$('.header-conversation').find('#friend-name').text(friendname);
		startConversation($(this));
		//}
		
	});

	$('#btn-close').on('click',function(){
		areYouSure();
		closeConversation();
	});

	function closeConversation(){
		$('.main-content-conversation').animate({'opacity': 0}, 400);
		$('.main-content-messages').text('');
		$(".active-edit").hide();

		prevActive(); prevEdit();

	}
	var ss='';
	function startConversation(e){
		ss= e.children('img').attr('src');
		// alert('nova '+ss);;
		$('.main-content-conversation').animate({'opacity': 1}, 400);	
		$('.input-group').children('input').val('');
	}

//ako ja ima klasata na drugo mesto da ja trgne
	function prevActive(){
		$('.active-conversation').removeClass('active-conversation');
	}
	function prevEdit(){
		
		$('.active-edit').children('.edit').hide();
		$('.active-edit').removeClass('active-edit');

	}
	function scrollDownToBottom(){
		$('.main-content-messages').scrollTop($('.main-content-messages')[0].scrollHeight);
	}
	var x;
	function sendMsg(){
		$('#btn-send').on('click',function(){
			sendMessage();
		});

		$('input').keypress(function (e) {
		  if (e.which == 13) {
		    sendMessage();
		    return false; 
		  }
		});

		// $("input").keyup(function(e){
		// console.log('test input');
		//    if(e.key==13){
		//    	alert('stiskam enter');
		// 		sendMessage();
		//    }
		// });
	}

	function sendMessage(){
		if($('.active-edit').find('button:visible').length>0)
		{
			x = $('.input-group').children('input').val();
			if(x!=''){
				$('.active-edit').find('.my-msg').text(x);
				$('.active-edit').find('#changed').text('edited');
			}
			
		}
		else
		{
			x = $('.input-group').children('input').val();
			if(x!==''){
				$('.test').text(x);
				///tuka generira poraka
				myMsg(x);
				generateMsg();
				scrollDownToBottom();
			}
		}

		$('.input-group').children('input').val('');
		$(".edit").hide();
	}

	function myMsg(x){
		$("<div>").attr({
		    'class': "my-message msg-container darker",
		}).appendTo(".main-content-messages");

		$("<img>").attr({
		    'src': './images/biljana.jpg',
		    'alt': 'avatar',
		    'class': 'left',
		}).appendTo($('.my-message').last());

		$('.my-message').last().append("<p><span class='my-msg'>"+x+"</span><span id='changed'></span></p>");
		$('.my-message').last().append("<p><span class='time-seen'> Today, "+getTime()+"</span> </p>");
		$('.my-message').last().append("<button class='btn btn-default edit'><span class='edit-btn'>EDIT</span></button>");
		
	}

	
	function generateMsg(){

		var greetings=['Hello',
					'Have a nice day!',
					"I'm having a wonderfull time. And you?",
					'Ohh, you look so nervous. Can I help you?',
					"I don't understand you. Can you repeat?",
					"Good job! You're doing very well"];
		var greetingsText = greetings[Math.floor(Math.random() * greetings.length)];
		$("<div>").attr({
		    'class': "my-message msg-container2 lighter",
		}).appendTo(".main-content-messages");
	

		$("<img>").attr({
		    'src': ss,
		    'alt': 'avatar',
		    'class': 'right',
		}).appendTo($('.my-message').last());


		$('.my-message').last().append("<p><span  id='bot-msg'>"+greetingsText+"</span></p>");
		$('.my-message').last().append("<p><span class='time-seen'> Today, "+getTime()+"</span> </p>");
	}
	sendMsg();

	function getTime(){
		var d = new Date();
		var time = d.getHours() + ":" + d.getMinutes();
		return time;
	}


	$(document).on('click','.my-message.darker',function(){

		prevEdit();
		$(this).addClass('active-edit');

		$(this).children(".edit").show();
	});

	$(document).on('click','.edit',function(){
	var dd='';
	dd= $(this).parent().find('.my-msg').text();
	if(dd!=='')
		$('.input-group').children('input').val(dd);
	});
	

});
